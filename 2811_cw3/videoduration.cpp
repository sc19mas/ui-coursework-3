#include "videoduration.h"

videoDuration::videoDuration(ThePlayer *player){
    play = player;
}

void videoDuration::disp(int number){
    number/= 1000;
    int h = number/3600;
    int m = (number-h*3600)/60;
    int s = number -(h*3600) - (m*60);
    QString displayVal = QString::number(m);
    if(s < 10){
        displayVal += ":";
        displayVal += "0";
        displayVal += QString::number(s);
        displayVal+="/";
    }
    else{
        displayVal += ":";
        displayVal += QString::number(s);
        displayVal+="/";
    }
    setText(displayVal);
}
void videoDuration::getDuration(qint64 duration){
    duration = play->duration();
    duration /= 1000;
    int h = duration/3600;
    int m = (duration-h*3600)/60;
    int s = duration -(h*3600) - (m*60);
    QString dur;
    if(m < 10){
        dur += "0";
        dur += ":";
        dur += QString::number(s);
        setText(dur);
    }
    else{
        dur += QString::number(m);
        dur += ":";
        dur += QString::number(s);
        setText(dur);
    }
}
