#ifndef VIDEOCOMMENT_H
#define VIDEOCOMMENT_H
#include <QTextEdit>
#include "the_player.h"

//THIS CLASS IS USED TO DISPLAY COMMENTS
//ADDED BY THE USER

class videoComment : public QTextEdit
{
    Q_OBJECT
public:
    videoComment(ThePlayer*);
    map<QString,QString>kComments{};//A MAP CORRESPONDING THE ID OF EACH VIDEO TO ITS TAG
public slots:
    void textPost();//ADDS THE COMMENTS TO THE MAP WHEN THE USER TYPES IN SOMETHING
    void textSet();//SETS THE TEXT OF THE TEXTEDIT TO "" WHEN THE VIDEO IS CHANGED
    void showComments();//SHOWS THE COMMENTS ADDED BY THE USER
private:
    ThePlayer* play;
    QString commentText;

};

#endif // VIDEOCOMMENT_H
