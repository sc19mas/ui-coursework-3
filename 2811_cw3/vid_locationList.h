#ifndef VID_LOCATIONLIST_H
#define VID_LOCATIONLIST_H

#include <QList>
#include <QString>
#include <vector>
#include "the_button.h"
#include "the_player.h"





//THIS IS THE NEW CONTAINER CLASS FOR ALL THE BUTTON INFO LOCATIONS ETC
//THIS WILL ALLOW THE VIDEOS DISPLAYED IN THE PLAYER AND IN THE SCROLL WIDGET TO CHANGE
//DURING THE DURATION OF THE PROGRAM
//THIS WILL ALSO ALLOW THE LOADING OF NEW DIRECTORIES, AND THE DELETION OF DIRECTORIES THE USER
//NO LONGER WANTS
//IN ORDER TO MAKE THIS DYNAMIC, SOME FUNCTIONS HAVE BEEN MOVED FROM TOMEO.CPP TO THIS CLASS

class ListLocations: public QWidget
{
    Q_OBJECT
public:
    QList<QString> locations;                       //list of strings read through vid_locations.txt
    ListLocations():QWidget(),LastLocation("C://"){}

    bool getInfo();                                     //loads info from list of strings
    bool LoadLocations();                                   //loads list of string locations from text file
    std::vector<TheButtonInfo> LoadIndividualLocation(QString);         //loads data from an individual location

    bool addAndSetPlayer(ThePlayer*,QWidget *, QWidget*,QLayout *);         //gets pointers to important widgets, and fills scroll widget with buttons


public slots:
    void OpenFile();                //opens a new folder as selected by the user
    void RemoveFile();              //removes folder as selected by the user
private:
    void deleteObjectData();            //deletes all objects
    QWidget * window;                   //new window to be used for selecting folder to remove
    std::vector<TheButtonInfo>*videos;      //dynamically allocated list of video information
    std::vector<TheButton *>* buttons;         //list of all the button widgets
    ThePlayer * player;                         //main video player
    QLayout * layout;                           //layout (to be used when removing folder)
    QWidget * buttonWidget;                     //widget that contains all buttons
    QWidget * containerWidget;                  //widget that contains scroll area
    QString LastLocation;                       //last accessed location - for convenience when choosing new location

private slots:
    void RemoveButtonInfoWithPath(QString);     //slot for button - for removing all videos of a specific location

};


#endif // VID_LOCATIONLIST_H
