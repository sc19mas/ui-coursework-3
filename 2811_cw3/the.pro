QT += core gui widgets multimedia multimediawidgets

CONFIG += c++11


# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        the_button.cpp \
        the_player.cpp \
        tomeo.cpp \
        videocomment.cpp \
        videocontrolbutton.cpp \
        videoduration.cpp \
        videoskip.cpp \
        videotag.cpp \
        vid_locationList.cpp

HEADERS += \
    the_button.h \
    the_player.h \
    videocomment.h \
    videocontrolbutton.h \
    videoduration.h \
    videoskip.h \
    videotag.h \
    vid_locationList.h

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc

FORMS += \
    mainwindow.ui

DISTFILES += \
    .gitignore \
    vid_locations.txt

