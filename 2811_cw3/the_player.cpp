//
// Created by twak on 11/11/2019.
//

#include "the_player.h"
#include <iostream>

using namespace std;

// all buttons have been setup, store pointers here
void ThePlayer::setContent(std::vector<TheButton*>* b, std::vector<TheButtonInfo>* i) {
    buttons = b;
    infos = i;
    jumpTo(buttons -> at(0) -> info);
}

// change the image and video for one button every one second
void ThePlayer::shuffle() {
    //TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
    //buttons -> at( updateCount++ % buttons->size() ) -> init( i );

    if (shuffleactive == true){
        TheButtonInfo* i = & infos -> at (rand() % infos->size() );
//        setMedia(*i->url);
        buttons -> at( updateCount++ % buttons->size() ) -> init( i );
    }
}
std::vector<TheButton*> *ThePlayer::getButtons(){
    return buttons;
}

bool ThePlayer::isShuffleActive(){
    return shuffleactive;
}

int ThePlayer::ButtonPosition(){
    QString titleText = title->text();

    for (int Count = 0;Count < int(buttons->size());Count++){
        if (getTitle(infos->at(Count).url->toString()).compare(titleText) == 0){
            return Count;
        }
    }
    return -1;
}


vector<TheButtonInfo>* ThePlayer::sortalphabetically(vector<TheButtonInfo>* videos){
    TheButtonInfo* temp;
    QString current, next;
    for (int i = 0; i < int(videos->size() - 1); i++ ) {
        for (int j = i; j < int(videos->size() - 1); j++){
            current = getTitle(videos->at(j).url->toString());
            next = getTitle(videos->at(j + 1).url->toString());
            if(current.compare(next) > 0){
                temp = &videos->at(j);
                videos->at(j) = videos->at(j+1);
                videos->at(j+1) = *temp;
            }
        }
    }
    return videos;
}

void ThePlayer::shuffleoff(){
    shuffleactive = false;
}

void ThePlayer::shuffleon(){
    shuffleactive = true;
}

void ThePlayer::sort(QString option){
    vector<TheButtonInfo>* i = infos;
    if(option == "Shuffle Videos"){
        shuffleactive = true;
    }
    else{
        shuffleactive = false;
        if(option == "Sort Alphabetically"){
            i = sortalphabetically(i);
            for (int j = 0; j < int(buttons->size()) ; j++ ) {
                buttons->at(j)->init(&i->at(j));
            }
        }
    }
}

void ThePlayer::playStateChanged (QMediaPlayer::State ms) {
    switch (ms) {
        case QMediaPlayer::State::StoppedState:
            play(); // starting playing again...
            break;
    default:
        break;
    }
}

void ThePlayer::pauseOrPlay(){
    std::cout << "state: " << state() << std::endl;
    std::cout << "CALLED" << std::endl;

    if (state() == QMediaPlayer::State::PlayingState){
        pause();
        if (playPause != NULL){
            playPause->setText("play");
        }
    }
    else {play();
        if (playPause != NULL){
            playPause->setText("pause");
        }}
}

QString ThePlayer::getTitle(QString str){
    QStringList strings = str.split("/");
    return strings.takeLast();
}


bool ThePlayer::isPlaying(){
    return (state() == QMediaPlayer::State::PlayingState) ;
}

void ThePlayer::jumpTo (TheButtonInfo* button) {
    setMedia( * button -> url);
    if (title != nullptr && id != nullptr){
        title->setText(getTitle(button->url->toString()));
        QString ID = QString::number(button->id);
        id->setText(ID);
        tag->setText(button->tag);
        comment->setText(button->comment);
    }
    else{
        std::cout << "no title" << std::endl;
        std::cout << "no id" <<std::endl;
    }
    play();
}
