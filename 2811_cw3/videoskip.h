#ifndef VIDEOSKIP_H
#define VIDEOSKIP_H
#include <QObject>
#include <QComboBox>

class videoSkip : public QComboBox
{
    Q_OBJECT
public:
    videoSkip(){};
public slots:
    void setIndex(int);//SETS THE INDEX OF THE VIDEO BACK TO 0 ONCE IT IS SKIPPED BY X SECONDS
};

#endif // VIDEOSKIP_H
