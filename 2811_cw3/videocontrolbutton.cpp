#include "videocontrolbutton.h"
#include "the_player.h"
#include <QPushButton>
#include <iostream>


videoControlButton::videoControlButton(VIDEO_BUTTON_TYPE type, ThePlayer * player = NULL):QPushButton(), currentPlayer(player)
{
    //CUSTOM SET UP FOR EACH BUTTON. THIS IS ONLY BASIC AND WILL BE EXPANDED UPON
    switch(type){
    case VIDEO_BUTTON_TYPE::PLAY_PAUSE:
        setText("Play/Pause");
        connect(this, SIGNAL(pressed()),this,SLOT(PausePlay()));
        break;
    case VIDEO_BUTTON_TYPE::FASTFORWARD:
        setText("Skip forward");
//        connect(this, SIGNAL(pressed()),this,SLOT(FastForward()));
//        connect(this, SIGNAL(released()),this,SLOT(ResetPlaySpeed()));
        break;
    case VIDEO_BUTTON_TYPE::REWIND:
        setText("Skip Back");
//        connect(this, SIGNAL(pressed()),this,SLOT(Rewind()));
//        connect(this, SIGNAL(released()),this,SLOT(ResetPlaySpeed()));
        break;
    case VIDEO_BUTTON_TYPE::NEXT:
        setText("Next");
        connect(this, SIGNAL(pressed()),this,SLOT(Next()));
        break;
    case VIDEO_BUTTON_TYPE::PREVIOUS:
        setText("Previous");
        connect(this, SIGNAL(pressed()),this,SLOT(Previous()));
        break;
    }
}

void videoControlButton::PausePlay(){
    std::cout << "Pause/Play" << std::endl;
    if (currentPlayer->state() == QMediaPlayer::State::PlayingState){
        currentPlayer->pause();
        setText("Pause");
    }
    else {currentPlayer->play();
    setText("Play");}
}
void videoControlButton::FastForward(QString speed){
    std::cout << "Fast Forward" << std::endl;
    qint64 currentPosition = currentPlayer->position();
    if(speed == "1x")
        ResetPlaySpeed();
    if(speed == "1.5x")
        currentPosition += 1500;
    if(speed == "2x")
        currentPosition += 2000;
    //currentPosition += 1000;
    //currentPlayer->setPlaybackRate(1.0);
    currentPlayer->setPosition(currentPosition);
    //currentPlayer->setVolume(100);
}
void videoControlButton::Rewind(QString speed){
    std::cout << "Rewind" << std::endl;
    qint64 currentPosition = currentPlayer->position();
    if(speed == "1x")
        ResetPlaySpeed();
    if(speed == "1.5x")
        currentPosition -= 1500;
    if(speed == "2x")
        currentPosition -= 2000;
    //currentPlayer->setPlaybackRate(1.0);
    currentPlayer->setPosition(currentPosition);
    //currentPlayer->setPlaybackRate(-1.0);
}
void videoControlButton::Next(){
    std::cout << "Next" << std::endl;
    if (currentPlayer->isShuffleActive())
    {
        currentPlayer->jumpTo(currentPlayer->getButtons() -> at(0) -> info);
    }
    else{
        int length = currentPlayer->getButtons()->size();
        int pos = currentPlayer->ButtonPosition();
        std::cout << "position: " << pos << std::endl;
        std::cout << "length: " << length << std::endl;
        pos = (pos + 1)%length;
        currentPlayer->jumpTo(currentPlayer->getButtons() -> at(pos) -> info);
    }
}

void videoControlButton::Previous(){std::cout << "Previous" << std::endl;
    std::cout << "Next" << std::endl;
    if (currentPlayer->isShuffleActive())
    {
        currentPlayer->jumpTo(currentPlayer->getButtons() -> at(0) -> info);
    }

    else{

        int length = currentPlayer->getButtons()->size();
        int pos = currentPlayer->ButtonPosition();
        std::cout << "position: " << pos << std::endl;
        std::cout << "length: " << length << std::endl;
        pos = (pos + length - 1)%length;
        currentPlayer->jumpTo(currentPlayer->getButtons() -> at(pos) -> info);
    }
}


void videoControlButton::ResetPlaySpeed(){
    std::cout << "Reset Speed" << std::endl;
    currentPlayer->setPlaybackRate(1.0);
}
