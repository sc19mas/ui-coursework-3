/*
 *
 *    ______
 *   /_  __/___  ____ ___  ___  ____
 *    / / / __ \/ __ `__ \/ _ \/ __ \
 *   / / / /_/ / / / / / /  __/ /_/ /
 *  /_/  \____/_/ /_/ /_/\___/\____/
 *              video for sports enthusiasts...
 *
 *  2811 cw3 : twak
 */

#include <iostream>
#include <QApplication>
#include <QtMultimediaWidgets/QVideoWidget>
#include <QMediaPlaylist>
#include <QScrollArea>
#include <string>
#include <vector>
#include <QComboBox>
#include <QtWidgets/QPushButton>
#include <QLCDNumber>
#include <QtWidgets/QHBoxLayout>
#include <QtCore/QFileInfo>
#include <QtWidgets/QFileIconProvider>
#include <QDesktopServices>
#include <QImageReader>
#include <QMessageBox>
#include <QtCore/QDir>
#include <QtCore/QDirIterator>
#include "the_player.h"
#include "the_button.h"
#include "videocontrolbutton.h"
#include "videotag.h"
#include "videocomment.h"
#include "vid_locationList.h"
#include <QFileDialog>
#include "videoskip.h"
#include "videoduration.h"
#include "ui_mainwindow.h"

using namespace std;

//Button data is now read in through ListLocations.loadLocations();, followed by ListLocations.getInfo();


int main(int argc, char *argv[]) {

    // let's just check that Qt is operational first
    qDebug() << "Qt version: " << QT_VERSION_STR << Qt::endl;

    // create the Qt Application
    QApplication app(argc, argv);

    // collect all the videos in the folder

    ListLocations * local = new ListLocations();
    local->setAttribute( Qt::WA_DeleteOnClose, true );
    local->LoadLocations();
    bool info = local->getInfo();

    if (!info) {

        const int result = QMessageBox::question(
                    NULL,
                    QString("Tomeo"),
                    QString("no videos found! download, unzip, and add command line argument to \"quoted\" file location. Download videos from Tom's OneDrive?"),
                    QMessageBox::Yes |
                    QMessageBox::No );

        switch( result )
        {
        case QMessageBox::Yes:
          QDesktopServices::openUrl(QUrl("https://leeds365-my.sharepoint.com/:u:/g/personal/scstke_leeds_ac_uk/EcGntcL-K3JOiaZF4T_uaA4BHn6USbq2E55kF_BTfdpPag?e=n1qfuN"));
          break;
        default:
            break;
        }
        exit(-1);
    }


      QMainWindow *window = new QMainWindow();
      Ui::MainWindow ui;
      ui.setupUi(window);
//    QGridLayout *grid = new QGridLayout();
//    ThePlayer *player = new ThePlayer;
//    // the widget that will show the video
//    QWidget *wid = new QWidget();
//    QWidget *window = new QWidget();
//    window->setMinimumSize(800, 680);
//    wid->setMinimumSize(250,1000);
//    wid->setMaximumSize(250,1000);
//    QVideoWidget *videoWidget = new QVideoWidget;

//    // the QMediaPlayer which controls the playback
//    player->setVideoOutput(videoWidget);
//    videoWidget->setMinimumHeight(100);
//    videoWidget->setMinimumWidth(50);
//    videoWidget->setMaximumWidth(1500);

//    // a row of buttons
//    QWidget *buttonWidget = new QWidget();
//    // a list of the buttons
//    vector<TheButton*> buttons;
//    // the buttons are arranged vertically
//    QVBoxLayout *layout = new QVBoxLayout();

//    buttonWidget->setLayout(layout);
//    wid->setLayout(layout);

//    //add a new label
//    QLabel * label = new QLabel();
//    player->title = label;
//    QFont f = label->font();
//    f.setPointSize(15);
//    label->setFont(f);
//    QLabel * id = new QLabel();
//    player->id = id;
//    grid->addWidget(id,0,0);

//    videoTag *tag = new videoTag(player);
//    videoComment *comment = new videoComment(player);

//    QHBoxLayout *topmost = new QHBoxLayout();

//    //BUTTONS ARE NOW CREATED THROUGH listLocations.addAndSetPlayer



//    local->addAndSetPlayer(player,buttonWidget,wid,layout);


//    videoControlButton *skipButton = new videoControlButton((VIDEO_BUTTON_TYPE)1, player);
//    videoControlButton *rewButton = new videoControlButton((VIDEO_BUTTON_TYPE)2,player);

//    // create the main window and layout
//    QHBoxLayout *top = new QHBoxLayout();
//    label->setAlignment(Qt::AlignCenter);
//    grid->addWidget(label,0,0);
//    QWidget *topWidget = new QWidget();
//    topWidget->setLayout(top);
//    grid->addWidget(topWidget,4,0);
//    window->setLayout(grid);

//    //Add a slider for the volume
//    QSlider *volume = new QSlider() ;
//    volume->setOrientation(Qt::Horizontal);
//    volume->setTickPosition(QSlider::TicksRight);
//    volume->setSingleStep(1);
//    volume->setMouseTracking(true);
//    volume->setTracking(true);
//    volume->setMinimum(0);
//    volume->setMaximum(100);
//    volume->setValue(0);
//    volume->setMaximumHeight(80);
//    volume->setMinimumWidth(80);
//    volume->setMaximumWidth(200);
//    grid->setAlignment(volume, Qt::AlignHCenter);
//    grid->addWidget(volume, 15, 0);
//    QObject::connect(volume, SIGNAL(sliderMoved(int)), player, SLOT(setVolume(int)));


//    QString ide = player->id->text();
//    qDebug()<<"ID is: "<<ide;
//    QLabel *l = new QLabel();
//    l->setFont(f);
//    player->tag = l;
//    grid->addWidget(l,0,0,Qt::AlignRight);
//    QPushButton *tagPush = new QPushButton("Show Tags");
//    QObject::connect(tagPush,SIGNAL(pressed()),tag,SLOT(showTags()));
//    window->setWindowTitle("tomeo");
//    QWidget *bottomWidget = new QWidget();
//    QHBoxLayout *bottom = new QHBoxLayout();
//    bottom->addWidget(comment);
//    QPushButton *commentPush = new QPushButton("Add Comment");
//    bottom->addWidget(commentPush);
//    bottomWidget->setLayout(bottom);
//    QObject::connect(commentPush,SIGNAL(pressed()),comment,SLOT(textPost()));

//    //Add a slider to track the video
//    videoDuration *number = new videoDuration(player);
//    videoDuration *totalDuration = new videoDuration(player);
//    QSlider *sly = new QSlider();
//    sly->setTickPosition(QSlider::NoTicks);
//    sly->setTabletTracking(true);
//    sly->setOrientation(Qt::Horizontal);
//    sly->setMouseTracking(true);
//    sly->setTracking(true);
//    sly->setMinimum(0);
//    QObject::connect(player,&QMediaPlayer::durationChanged,sly,&QSlider::setMaximum);
//    sly->setMinimumWidth(300);
//    sly->setSliderDown(false);
//    sly->setMaximumWidth(500);
//    QObject::connect(sly,&QSlider::sliderPressed,player,&QMediaPlayer::pause);
//    QObject::connect(sly,&QSlider::sliderReleased,player,&QMediaPlayer::play);
//    QObject::connect(player,&QMediaPlayer::positionChanged,sly,&QSlider::setValue);
//    //QObject::connect(sly,&QSlider::valueChanged,player,&QMediaPlayer::setPosition);
//    QObject::connect(sly,SIGNAL(valueChanged(int)),number,SLOT(disp(int)));
//    QObject::connect(player,SIGNAL(durationChanged(qint64)),totalDuration,SLOT(getDuration(qint64)));
//    QHBoxLayout *middle = new QHBoxLayout();
//    QWidget *middleWidget = new QWidget();
//    number->setMaximumWidth(100);
//    middleWidget->setLayout(middle);
//    middle->addWidget(number);
//    middle->addWidget(totalDuration);
//    middle->addWidget(sly,Qt::AlignCenter);
//    middle->setContentsMargins(0,0,400,0);
//    grid->addWidget(middleWidget,13,0);

//    QLabel *commentLabel = new QLabel();
//    commentLabel->setFont(f);
//    player->comment = commentLabel;
//    grid->addWidget(commentLabel,21,0);
//    QPushButton *showComments = new QPushButton("Show all comments");
//    bottom->addWidget(showComments);
//    QObject::connect(showComments,SIGNAL(clicked()),comment,SLOT(showComments()));
//    grid->addWidget(bottomWidget,20,0);

//    //create the combobox for sorting options
//    QWidget *topmostWidget = new QWidget();
//    QComboBox *combo = new QComboBox();
//    combo->setMaximumWidth(200);
//    combo->addItem("Sort Alphabetically");
//    combo->addItem("Shuffle Videos");
//    QObject::connect(combo, SIGNAL(currentTextChanged(QString)), player, SLOT(sort(QString)));
//    topmost->addWidget(combo);
//    topmost->addWidget(tag);
//    topmost->addWidget(tagPush);
//    topmostWidget->setLayout(topmost);
//    grid->addWidget(topmostWidget,1,0);

//    // add the video and the buttons to the top level widget
//    top->addWidget(videoWidget);
//    tag->setMinimumWidth(400);
//    //add a new button
//    QPushButton * button =  new QPushButton ("pause");
//    player->playPause = button;
//    QObject::connect(button,SIGNAL(pressed()),player,SLOT(pauseOrPlay()));

//    QHBoxLayout * buttonsLayout = new QHBoxLayout();
//    QWidget * speedControl = new QWidget();
//    videoControlButton *previous = new videoControlButton((VIDEO_BUTTON_TYPE)4,player);
//    videoControlButton *next = new videoControlButton((VIDEO_BUTTON_TYPE)3,player);
//    buttonsLayout->addWidget(previous);
//    previous->setMaximumWidth(60);
//    previous->setMaximumHeight(50);

//    videoSkip *skip = new videoSkip();
//    skip->addItem("1x");
//    skip->addItem("1.5x");
//    skip->addItem("2x");
//    QObject::connect(skip,SIGNAL(currentTextChanged(QString)),skipButton,SLOT(FastForward(QString)));
//    QObject::connect(sly,SIGNAL(valueChanged(int)),skip,SLOT(setIndex(int)));

//    videoSkip *rew = new videoSkip();
//    rew->addItem("1x");
//    rew->addItem("1.5x");
//    rew->addItem("2x");
//    QObject::connect(rew,SIGNAL(currentTextChanged(QString)),rewButton,SLOT(Rewind(QString)));
//    QObject::connect(sly,SIGNAL(valueChanged(int)),rew,SLOT(setIndex(int)));
//    buttonsLayout->addWidget(rew);

//    buttonsLayout->addWidget(button);
//    button->setMaximumWidth(50);
//    button->setMinimumHeight(65);
//    buttonsLayout->addWidget(skip);

//    skip->setMaximumWidth(50);
//    skip->setMaximumHeight(50);

//    rew->setMaximumWidth(50);
//    rew->setMaximumHeight(50);

//    buttonsLayout->addWidget(next);
//    next->setMaximumWidth(60);
//    next->setMaximumHeight(50);
//    buttonsLayout->setSpacing(0);

//    speedControl->setLayout(buttonsLayout);
//    grid->addWidget(speedControl,15,0);
//    grid->setAlignment(speedControl,Qt::AlignHCenter);
//    top->addWidget(buttonWidget);

//    QObject::connect(next,SIGNAL(pressed()),tag,SLOT(textChange()));
//    QObject::connect(previous,SIGNAL(pressed()),tag,SLOT(textChange()));
//    QObject::connect(next,SIGNAL(pressed()),comment,SLOT(textSet()));
//    QObject::connect(previous,SIGNAL(pressed()),comment,SLOT(textSet()));

//    QHBoxLayout *topMiddle = new QHBoxLayout();
//    QPushButton * chooseNewFile = new QPushButton("Choose File");
//    QObject::connect(chooseNewFile,SIGNAL(pressed()),local,SLOT(OpenFile()));
//    topMiddle->addWidget(chooseNewFile);

//    QPushButton * removeFile = new QPushButton("Remove files");
//    QObject::connect(removeFile,SIGNAL(pressed()),local,SLOT(RemoveFile()));
//    topMiddle->addWidget(removeFile);

//    QWidget *topMiddleWidget = new QWidget();
//    topMiddleWidget->setLayout(topMiddle);
//    grid->addWidget(topMiddleWidget,3,0,Qt::AlignRight);

//    QScrollArea *scroll = new QScrollArea();
//    scroll->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    scroll->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
//    scroll->setWidget(wid);
//    scroll->setAlignment(Qt::AlignCenter);
//    scroll->setMaximumWidth(400);
//    scroll->setMinimumWidth(260);
//    top->addWidget(scroll);
//    top->setContentsMargins(0,10,0,0);
//    // showtime!
    window->show();
    // wait for the app to terminate
    return app.exec();
}
