#include "videocomment.h"
#include <QMessageBox>
videoComment::videoComment(ThePlayer* player)
{
    play = player;
    setPlaceholderText("Comment");

}
void videoComment::textPost(){
    commentText = this->toPlainText();
    //qDebug()<<commentText;
    QString id = play->id->text();
    kComments.insert(make_pair(id,commentText));
    play->comment->setText(kComments.at(play->id->text()));
    //qDebug()<<kComments;
    setText("");
}
void videoComment::textSet(){
    setText("");
    setReadOnly(false);
}
void videoComment::showComments(){
    if(kComments.empty()){
        QMessageBox *box = new QMessageBox();
        box->setText("No comments to display!");
        box->exec();
    }
    else{
        play->comment->setText(kComments.at(play->id->text()));
    }
}
