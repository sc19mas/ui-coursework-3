#include "vid_locationList.h"
#include <iostream>
#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
#include <QDir>
#include <QDirIterator>
#include <QImageReader>
#include <QMessageBox>
#include <QFileDialog>
#include <QWidget>
#include <QLayout>
#include <QVBoxLayout>
#include "videotag.h"

bool ListLocations::LoadLocations(){
    std::ifstream stream("../2811_cw3/vid_locations.txt");
    if (stream.is_open()){
        std::cout << "opened" << std::endl;
        std::string line;
        while(std::getline(stream,line)){
            locations.append(QString::fromStdString(line).trimmed());
        }

        for (QString str: locations){
            std::cout << str.toStdString() << std::endl;
        }
        LastLocation = locations.last();
        stream.close();
    }
    else{
        std::cout << "not opened!!" << std::endl;
        return false;
    }

    return true;
}


std::vector<TheButtonInfo> ListLocations::LoadIndividualLocation(QString local){
    std::vector<TheButtonInfo> out =  std::vector<TheButtonInfo>();
    QDir dir(local );
    QDirIterator it(dir);
    ThePlayer *player = new ThePlayer;
    videoTag *tag = new videoTag(player);
    while (it.hasNext()) { // for all files

        QString f = it.next();

            if (f.contains("."))

    #if defined(_WIN32)
            if (f.contains(".wmv"))  { // windows
    #else
            if (f.contains(".mp4") || f.contains("MOV"))  { // mac/linux
    #endif

            QString thumb = f.left( f .length() - 4) +".png";
            if (QFile(thumb).exists()) { // if a png thumbnail exists
                QImageReader *imageReader = new QImageReader(thumb);
                    QImage sprite = imageReader->read(); // read the thumbnail
                    if (!sprite.isNull()) {
                        QIcon* ico = new QIcon(QPixmap::fromImage(sprite)); // voodoo to create an icon for the button
                        QUrl* url = new QUrl(QUrl::fromLocalFile( f )); // convert the file location to a generic url
                        qint64 id = out.size();
                        QLabel *tagLabel = new QLabel();
                        QObject::connect(tag,&QLineEdit::textChanged,tagLabel,&QLabel::setText);
                        QString tagger = tagLabel->text();
                        QLabel *commentLabel = new QLabel();
                        QString commenter = commentLabel->text();
                        out . push_back(TheButtonInfo(url , ico ,id, tagger, commenter) ); // add to the output list
                    }
                    else
                        qDebug() << "warning: skipping video because I couldn't process thumbnail " << thumb << Qt::endl;
            }
            else
                qDebug() << "warning: skipping video because I couldn't find thumbnail " << thumb << Qt::endl;
        }

    }
    return out;

}
//RELOCATE THE CODE FROM TOMEO.CPP INTO CLASS
bool ListLocations::getInfo(){

    videos =  new std::vector<TheButtonInfo>();
    for (QString local:locations)
    {
        std::vector<TheButtonInfo> currentLocal = LoadIndividualLocation(local);
        videos->insert(videos->end(),currentLocal.begin(),currentLocal.end());
    }
    return videos->size() > 0;
}

//
bool ListLocations::addAndSetPlayer(ThePlayer * pl, QWidget * bw, QWidget * cw, QLayout * l){
    player = pl;
    layout = l;
    buttonWidget = bw;
    containerWidget = cw;

    buttons = new vector<TheButton*>();
    // create the four buttons
    for ( int i = 0; i < (int)videos->size(); i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons->push_back(button);
        button->setMinimumSize(150,150);
        layout->addWidget(button);
        button->init(&videos->at(i));
    }
    // tell the player what buttons and videos are available
    //std::cout << "Layout size contraint" << layout->sizeConstraint() << std::endl;

    //layout->setSizeConstraint(QLayout::SetNoConstraint);
    containerWidget->setMinimumSize(QSize(150,150 * videos->size()));
    containerWidget->setMaximumSize(QSize(250,200 * videos->size()));


    player->setContent(buttons, videos);

    return true;
}


void ListLocations::OpenFile(){
    QString filename = QFileDialog::getExistingDirectory(this,"Open a file",LastLocation, QFileDialog::ShowDirsOnly);

    if (filename != NULL)
    {
        std::vector<TheButtonInfo> currentLocal = LoadIndividualLocation(filename);
        if (currentLocal.size() == 0){
            QMessageBox::warning(this,"File Error","No videos were found in this location");
            return;

        }
        else if (locations.contains(filename)){
            QMessageBox::warning(this,"File Error","This directory is already available");
            return;
        }

        videos->insert(videos->end(),currentLocal.begin(),currentLocal.end());


        for ( int i = videos->size() - currentLocal.size(); i < (int)videos->size(); i++ ) {
            TheButton *button = new TheButton(buttonWidget);
            button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
            buttons->push_back(button);
            //button->setMinimumSize(150,150);
            layout->addWidget(button);
            button->init(&videos->at(i));
        }

        player->setContent(buttons, videos);

        containerWidget->setMinimumSize(QSize(150,150 * videos->size()));
        containerWidget->setMaximumSize(QSize(250,200 * videos->size()));

        locations.append(filename);
        std::ofstream stream("../2811_cw3/vid_locations.txt");
        if (stream.is_open()){
            for (QString local: locations){
                stream << local.toStdString() << std::endl;
            }
            stream.close();
        }
    }
    else{
        std::cout << "No files were selected" << std::endl;
    }

}

void ListLocations::RemoveButtonInfoWithPath(QString path){
    if (window != nullptr){
        window->close();
        window = NULL;
    }

    //std::cout << path.toStdString() << std::endl;
    //return;

    vector<TheButtonInfo> newVids;

    for (TheButtonInfo bi: *videos){
        QString currentPath = (path + "/" + bi.url->toString().split("/").takeLast());
        QString buttonInfoUrl = bi.url->toString().replace("file:","").replace("///","");
        if (currentPath.compare(buttonInfoUrl) != 0){

            newVids.push_back(bi);
        }
        else{
            std::cout << "wow" << std::endl;
        }
    }
    videos->clear();
    for (TheButtonInfo bi: newVids){
        videos->push_back(bi);
    }

    locations.removeOne(path);

    for (TheButton * b: *buttons){
        delete b;
    }
    buttons->clear();


    for (QString local:locations){
        std::cout << local.toStdString() << std::endl;
    }

    for (TheButtonInfo bi: *videos){
        std::cout << bi.url->toString().toStdString() << std::endl;
    }

    for ( int i = 0; i < (int)videos->size(); i++ ) {
        TheButton *button = new TheButton(buttonWidget);
        button->connect(button, SIGNAL(jumpTo(TheButtonInfo* )), player, SLOT (jumpTo(TheButtonInfo* ))); // when clicked, tell the player to play.
        buttons->push_back(button);
        button->setMinimumSize(150,150);
        layout->addWidget(button);
        button->init(&videos->at(i));
    }

    std::ofstream stream("../2811_cw3/vid_locations.txt");
    if (stream.is_open()){
        for (QString local: locations){
            stream << local.toStdString() << std::endl;
        }
        stream.close();
    }
    player->setContent(buttons, videos);

    containerWidget->setMinimumSize(QSize(150,150 * videos->size()));
    containerWidget->setMaximumSize(QSize(250,200 * videos->size()));

}

void ListLocations::RemoveFile(){
    if (locations.size() <= 1){
        QMessageBox::warning(this,"File Error","Deleting a directory will delete all videos. Please select a new directory before you want to"
                             "delete anything");
        return;
    }

    std::cout << "Remove file" << std::endl;
    window =new QWidget();
    window->setWindowTitle("Remove File From List");
    QVBoxLayout * layout = new QVBoxLayout();
    layout->addWidget(new QLabel("Test Label"));
    for (QString local:locations){
        QPushButton * pb = new QPushButton(local);
        connect(pb,&QPushButton::pressed,this,[=]() {RemoveButtonInfoWithPath(local); });
        layout->addWidget(pb);
    }

    window->setLayout(layout);


    window->setMinimumSize(600, (locations.size() + 1)*60);
    window->show();


}

void ListLocations::deleteObjectData(){
    for (TheButton * b: *buttons){
        delete b;
    }
}
