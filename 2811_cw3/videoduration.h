#ifndef VIDEODURATION_H
#define VIDEODURATION_H
#include <QObject>
#include <QLCDNumber>
#include <QLabel>
#include "the_player.h"

class videoDuration: public QLabel
{
    Q_OBJECT
public:
    videoDuration(ThePlayer*);
public slots:
    void disp(int);//DISPLAYS THE CURRENT DURATION OF THE VIDEO IN SECONDS
    void getDuration(qint64);//DISPLAYS THE TOTAL LENGTH OF THE VIDEO
private:
    ThePlayer *play;
};

#endif // VIDEODURATION_H
