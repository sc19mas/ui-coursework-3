#include "videotag.h"
#include <QMessageBox>
videoTag::videoTag(ThePlayer *play)
{
    player = play;
    connect(this,SIGNAL(returnPressed()),this,SLOT(textEdit()));
    setPlaceholderText("Enter tag here, for eg: #elephant. Press 'Enter' to save");
}
void videoTag::textEdit(){
  tagText = text();
  QString id = player->id->text();
  kTags.insert(make_pair(id,tagText));
  player->tag->setText(kTags.at(player->id->text()));
  setReadOnly(true);
}
void videoTag::textChange(){
    setText("");
    setReadOnly(false);
}
void videoTag::showTags(){
    QMessageBox *messageBox = new QMessageBox();
    if(kTags.empty()){
        messageBox->setText("No Tags to display!");
        messageBox->exec();
    }
    else{
        player->tag->setText(kTags.at(player->id->text()));
    }
}
