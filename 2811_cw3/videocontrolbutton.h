#ifndef VIDEOCONTROLBUTTON_H
#define VIDEOCONTROLBUTTON_H

#include <QWidget>
#include <QPushButton>
#include <QLayout>
#include "the_player.h"

typedef enum {
    PLAY_PAUSE,
    FASTFORWARD,
    REWIND,
    NEXT,
    PREVIOUS
}VIDEO_BUTTON_TYPE;

class videoControlButton: public QPushButton
{
    Q_OBJECT
public:
    videoControlButton(VIDEO_BUTTON_TYPE, ThePlayer *);
    ThePlayer * currentPlayer;

private:
    qint64 rate;

private slots:
    void PausePlay();
    void FastForward(QString speed);
    void Rewind(QString speed);
    void Next();
    void Previous();
    void ResetPlaySpeed();
};

#endif // VIDEOCONTROLBUTTON_H
