#ifndef VIDEOTAG_H
#define VIDEOTAG_H
#include <QLineEdit>
#include "the_player.h"


class videoTag : public QLineEdit
{
    Q_OBJECT
public:
    videoTag(ThePlayer*);
    map<QString,QString>kTags{};//A MAP CORRESPONDING THE ID OF THE VIDEO TO ITS CORRESPONDING TAG
private:
    ThePlayer *player;
    QString tagText;
public slots:
    void textChange();//SETS THE TEXT IN THE LINEEDIT TO "" WHEN THE VIDEO IS CHANGED
    void showTags();//DISPLAYS THE TAGS ADDED BY THE USER
private slots:
    void textEdit();//ADDS THE TAG INPUTTED BY THE USER TO THE MAP
};

#endif // VIDEOTAG_H
