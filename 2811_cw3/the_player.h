//
// Created by twak on 11/11/2019.
//

#ifndef CW2_THE_PLAYER_H
#define CW2_THE_PLAYER_H


#include <QApplication>
#include <QMediaPlayer>
#include "the_button.h"
#include <vector>
#include <QTimer>
#include <QLabel>

using namespace std;

class videoTag;

class ThePlayer : public QMediaPlayer {

Q_OBJECT

private:
    vector<TheButtonInfo>* infos;
    vector<TheButton*>* buttons;
    QTimer* mTimer;
    long updateCount = 0;
    bool shuffleactive = false;

public:

    QLabel *tag;
    QLabel *comment;
    ThePlayer() : QMediaPlayer(NULL), title(nullptr) {
        setVolume(0); // be slightly less annoying
        tag = new QLabel();
        comment = new QLabel();
        connect (this, SIGNAL (stateChanged(QMediaPlayer::State)), this, SLOT (playStateChanged(QMediaPlayer::State)) );
        mTimer = new QTimer(NULL);
        mTimer->setInterval(1000); // 1000ms is one second between ...
        mTimer->start();
        connect( mTimer, SIGNAL (timeout()), SLOT ( shuffle() ) ); // ...running shuffle method
    }

    // all buttons have been setup, store pointers here
    void setContent(vector<TheButton*>* b, vector<TheButtonInfo>* i);
    QLabel * title;
    QPushButton *playPause;
    std::vector<TheButton*>*getButtons();
    QLabel *id;
    bool isShuffleActive();
    int ButtonPosition();
private:
    bool isPlaying();
    QString getTitle(QString);
private slots:

    // change the image and video for one button every one second
    void shuffle();
    void playStateChanged (QMediaPlayer::State ms);


public slots:
    void pauseOrPlay();
    // start playing this ButtonInfo
    void jumpTo (TheButtonInfo* button);
    vector<TheButtonInfo>* sortalphabetically(vector<TheButtonInfo>*);
    void shuffleoff();
    void shuffleon();
    void sort(QString option);
};

#endif //CW2_THE_PLAYER_H
